﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThumbnailManager:MonoBehaviour {

	public Transform container;
	public GameObject prefab;

    public UIGrid grid;

    private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();
	
	void Start () {

        fitScreen();
        createThumbnailVOList();
		createThumbnailPrefabs();
        enableScroll();
    }

    // Method designed in order to fit the screen resolution
    private void fitScreen()
    {
        CanvasScaler canvasScaler = GetComponentInParent<CanvasScaler>();
        float scaleFactor = canvasScaler.referenceResolution.x / Screen.width;


        RectTransform managerRectTrans = GetComponent<RectTransform>();
        managerRectTrans.sizeDelta = new Vector2(canvasScaler.referenceResolution.x, Screen.height * scaleFactor);

        RectTransform containerRectTrans = container.GetComponent<RectTransform>();
        containerRectTrans.sizeDelta = new Vector2(managerRectTrans.rect.width  - managerRectTrans.rect.width  * 0.04f,
                                                   managerRectTrans.rect.height - managerRectTrans.rect.height * 0.04f);
        containerRectTrans.localPosition = new Vector3(-containerRectTrans.rect.width / 2.0f, containerRectTrans.rect.height / 2.0f, 0);
    }

	private void createThumbnailVOList() {
		ThumbnailVO thumbnailVO;
		for (int i=0; i<1000; i++) {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }

        // Added a special case used in order to turn the out of screen thumbnails transparent
        thumbnailVO = new ThumbnailVO();
        thumbnailVO.id = "-1";
        _thumbnailVOList.Add(thumbnailVO);
    }

	private void createThumbnailPrefabs() {

        grid.init(container.GetComponent<RectTransform>().rect.width, container.GetComponent<RectTransform>().rect.height);
        grid.initRows(prefab, container, _thumbnailVOList);
       
    }

    private void enableScroll()
    {
        ScrollManager.instance.init(container.localPosition.y, grid.getMaximumScrollingY(1000));
    }

}
