﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Thumbnail:MonoBehaviour {

	private ThumbnailVO _thumbnailVO; 
	
	public ThumbnailVO thumbnailVO {
		get {
			return _thumbnailVO;
		}
		set {
			_thumbnailVO = value;
            if (_thumbnailVO.id != "-1")
            {
                GetComponent<Image>().sprite = SpriteManager.instance.GetSprite(_thumbnailVO.id);
                GetComponent<Image>().color = Color.white;
                GetComponentInChildren<Text>().text = _thumbnailVO.id.ToString();
            }
            else // Added a special case in order to make the thumbnails transparent when out of range
            {
                GetComponentInChildren<Text>().text = "";
                GetComponent<Image>().color = Color.clear;
            }
		}
	}

	public void OnClick() {
		Debug.Log("Thumbnail clicked: "+_thumbnailVO.id);
	}
}