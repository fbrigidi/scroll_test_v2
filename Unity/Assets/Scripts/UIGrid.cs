﻿/*
 *  The Grid is responsible of the tumbnails pooling. It is designed in order to limit the number of instanced objects.
 *  The Grid separates the thumbnails into UIRows and pools them in front of the scrolling direction
 *
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UIGrid : MonoBehaviour
{
    // Public interface

    public int cellSizeX;
    public int cellSizeY;

    public int spacingX;
    public int spacingY;

    public GameObject rowPrefab;

    // Internal variables

    UIRow[] rows;
    LinkedList<int> rowsIdx;
    int minimumVisibleRowIdx;

    int columnsNbr;     // the number of columns is equal to the one visible on the screen
    int rowsNbr;        // the number of rows is equal to the one visible on the screen plus 4 rows used as buffer

    float thumbnailPanelHeight;

    Transform container;
    float containerShift;

    // intialize the grid setting the dimensions
    public void init( float width, float height)
    {
        thumbnailPanelHeight = height;

        columnsNbr  = (int)Mathf.Round(width / (cellSizeX + spacingX)) ;
        rowsNbr     = (int)Mathf.Round(height / (cellSizeY + spacingY)) + 4;

        minimumVisibleRowIdx = 0;
    }

    // creates and initialize the rows
    public void initRows(GameObject thumbnailPrefab, Transform container, List<ThumbnailVO> _thumbnailVOList)
    {
        this.container = container;
        rows    = new UIRow[rowsNbr];
        rowsIdx = new LinkedList<int>(Enumerable.Range(0, rowsNbr));

        Vector3 thumbnailsGridOffset = (Vector3.left * ((columnsNbr - 1) * (spacingY + cellSizeY)) / 2.0f) +
            (Vector3.right * container.GetComponent<RectTransform>().rect.width / 2.0f);

        GameObject gameObj;
        for (int i = 0; i < rowsNbr; i++)
        {
            gameObj = (GameObject)Instantiate(rowPrefab);
            rows[i] = gameObj.GetComponent<UIRow>();
            
            rows[i].init(columnsNbr, spacingY + cellSizeY, thumbnailsGridOffset, thumbnailPrefab, new Vector2(cellSizeX, cellSizeY), _thumbnailVOList);
            rows[i].transform.SetParent(container, false);

            rows[i].updateRow(i - 2, -( i - 2 + 0.5f) * (spacingY + cellSizeY));
            
        }

        StartCoroutine(UpdateRowsCO());

    }

    // returns the maximum scrolling height according to the grid size
    public float getMaximumScrollingY( int thumbnailsNbr )
    {
        int totalRowsNbr      = thumbnailsNbr / columnsNbr;
        float totalRowsHeight = totalRowsNbr * (cellSizeY + spacingY);

        return totalRowsHeight - thumbnailPanelHeight;
    }

    // pulls the lowest row to the topmost position
    private IEnumerator pullRowUp()
    {
        minimumVisibleRowIdx--;
        rows[rowsIdx.Last.Value].updateRow(minimumVisibleRowIdx - 2, rows[rowsIdx.First.Value].transform.localPosition.y + (cellSizeY + spacingY));
        rowsIdx.ShiftLeft();
        
        yield return null;
    }

    // pulls the topmost row to the lowest position
    private IEnumerator pullRowDown()
    {
        minimumVisibleRowIdx++;
        rows[rowsIdx.First.Value].updateRow(minimumVisibleRowIdx + (rowsNbr-3), rows[rowsIdx.Last.Value].transform.localPosition.y - (cellSizeY + spacingY) );
        rowsIdx.ShiftRight();
       
        yield return null;
    }

    // coroutine updating the row positioning 
    IEnumerator UpdateRowsCO()
    {
        float currentContainerY = container.transform.localPosition.y;
        float startingContainerY = currentContainerY;

        int totalShifts = 0;

        while (true)
        {
            currentContainerY = container.transform.localPosition.y;
            containerShift = currentContainerY - startingContainerY;

            if (containerShift >= (totalShifts + 1) * (cellSizeY + spacingY))
            {
                yield return StartCoroutine(pullRowDown());
                totalShifts++;     
            }
            else if (containerShift <= (totalShifts - 1) * (cellSizeY + spacingY))
            { 
                yield return StartCoroutine(pullRowUp());
                totalShifts--;             
            }
            else { yield return null; }
            
        }
    }

}
