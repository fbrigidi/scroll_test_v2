﻿/*
 * Extension created in order to perform index shifts using int arrays
 * 
 * */

public static class ArrayShiftExtension
{
    public static int[] ShiftRight(this int[] array)
    {
        int[] tArray = new int[array.Length];
        for (int i = 0; i < array.Length; i++)
        {
            if (i < array.Length - 1)
                tArray[i] = array[i + 1];
            else
                tArray[i] = array[0];
        }
        return tArray;
    }

    public static int[] ShiftLeft(this int[] array)
    {
        int[] tArray = new int[array.Length];
        for (int i = array.Length - 1; i >= 0 ; i--)
        {
            if (i > 0)
                tArray[i] = array[i - 1];
            else
                tArray[i] = array[array.Length - 1];
        }
        return tArray;
    }
}
