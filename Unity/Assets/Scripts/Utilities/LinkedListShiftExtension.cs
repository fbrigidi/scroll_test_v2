﻿/*
 * Extension created in order to perform index shifts using a Linked List of int
 * 
 * */

using System.Collections.Generic;

public static class LinkedListShiftExtension
{
    public static void ShiftRight(this LinkedList<int> list)
    {
        int v = list.First.Value;
        list.RemoveFirst();
        list.AddLast(v);
    }

    public static void ShiftLeft(this LinkedList<int> list)
    {
        int v = list.Last.Value;
        list.RemoveLast();
        list.AddFirst(v);
    }
}