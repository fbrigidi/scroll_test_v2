﻿/*
 * Input manager dealing with Touch inputs
 * */

using UnityEngine;

public class TouchInputManager : InputManager
{
    private void Update()
    {
        if (Input.touchCount > 0)
        {
            pressed = true;
        }

        if (pressed)
        {
            pressPosition.Set(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
        }

        if (Input.GetTouch(0).phase == TouchPhase.Ended && pressed)
        {
            pressed = false;
        }

    }
}
