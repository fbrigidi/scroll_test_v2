﻿/*
 * Input manager dealing with Mouse inputs
 * */

using UnityEngine;

public class MouseInputManager : InputManager
{
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            pressed = true;
        }

        if (pressed)
        {
            pressPosition.Set(Input.mousePosition.x, Input.mousePosition.y);
        }

        if (Input.GetMouseButtonUp(0))
        {
            pressed = false;
        }

    }
}
