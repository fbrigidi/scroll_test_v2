﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    protected bool pressed = false;

    protected Vector2 pressPosition;

    public bool IsPressed()
    {
        return pressed;
    }

    public Vector2 GetPosition()
    {
        return pressPosition;
    }

}
