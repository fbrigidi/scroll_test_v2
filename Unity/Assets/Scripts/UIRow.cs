﻿/*
 *  Defines a row of thumbnails.
 *  It is the object subject of the pooling
 *
 */

using System.Collections.Generic;
using UnityEngine;

public class UIRow:MonoBehaviour
{
    Thumbnail[] thumbnails;

    List<ThumbnailVO> thumbnailVOList;

    public void init(int thumbnailsNbr, float vertSpacing, Vector3 offset,
        GameObject prefab, Vector2 thumbnailSize, List<ThumbnailVO> _thumbnailVOList)
    {
        thumbnails = new Thumbnail[thumbnailsNbr];
        thumbnailVOList = _thumbnailVOList;

        GameObject gameObj;
        RectTransform rectTrans;
        for (int i = 0; i < thumbnailsNbr; i++)
        {
            gameObj = (GameObject)Instantiate(prefab);
            rectTrans = gameObj.GetComponent<RectTransform>();

            rectTrans.position = Vector3.right * i * vertSpacing + offset;
            rectTrans.sizeDelta = thumbnailSize;

            gameObj.transform.SetParent(transform, false);
            
            thumbnails[i] = gameObj.GetComponent<Thumbnail>();

        }
    }

    public void updateRow ( int rowNbr, float yPosition )
    {
        transform.localPosition = Vector3.up * yPosition;
        int idx = 0;
        for (int i = 0; i < thumbnails.Length; i++)
        {
            idx = (rowNbr * thumbnails.Length) + i;
            if (idx >= 0 && idx < thumbnailVOList.Count)
            {
                thumbnails[i].thumbnailVO = thumbnailVOList[idx];
            }
            else { thumbnails[i].thumbnailVO = thumbnailVOList[1000]; }
        }
    }

}
