﻿/*
 * This component is controlling the thumbnails scrolling
 * At runtime the component is instancing the input controller depending on platform 
 * It is implemented as a singleton
 * 
 * */
using UnityEngine;

public class ScrollManager : MonoBehaviour {

    InputManager input;

    public static ScrollManager instance
    {
        get; private set;
    }

    // Scrolling behaviour parameters

    public float speed;
    public float speedDump;

    [Range(1, 10)]
    public float fastScrollingThreshold;

    // Scrolling Limits

    float minY;
    float maxY;

    bool upperLimitSwitch;
    bool lowerLimitSwitch;

    // Internal variables

    bool isEnabled;     // enables the thumbnails scrolling

    float currentY;     // temp variables
    float previousY;
    float offsetY;

    bool pressed;       // true when the player is holding down the mouse btn or pressing a finger on the scrren

    bool fastScrolling; // true when the player is scrolling fast, enables the inertial scrolling
    float fastScrollingLength = 0;

    void Awake()
    {
        // Sinigleton implementation
        if (instance == null) { instance = this; }
        else
        {
            if (this != instance) { Destroy(this.gameObject); }
        }

        // input system initialization
#if UNITY_EDITOR
        input = gameObject.AddComponent<MouseInputManager>();
#else
        input = gameObject.AddComponent<TouchInputManager>();
#endif

        // variables initialization
        pressed             = false;
        fastScrolling       = false;
        upperLimitSwitch    = false;
        lowerLimitSwitch    = false;
    }

    // scroll initialization
    public void init(float MinY, float MaxDeltaY)
    {
        minY = MinY;
        maxY = MinY + MaxDeltaY;

        isEnabled = true;
    }

    // switches on and of the pressed status
    private void pressedStatusSwitch()
    {
        if (!pressed)
        {
            pressed = true;
            previousY = input.GetPosition().y;
            offsetY = input.GetPosition().y - transform.localPosition.y;
        }
        else
        {
            pressed = false;
        }
    }

    // scrolls the thumbnails to position
    private void scroll()
    {
        currentY = input.GetPosition().y;
        Vector3 targetLocation = transform.localPosition + Vector3.up * (currentY - offsetY - transform.localPosition.y) * speed * Time.deltaTime;
        checkTargetLocation(ref targetLocation);
        transform.localPosition = targetLocation;
    }

    // scrolls the thumbnails until the speed falls below the threshold
    private void inertialScroll()
    {
        Vector3 targetLocation = transform.localPosition + Vector3.up * fastScrollingLength * speed * Time.deltaTime;
        checkTargetLocation(ref targetLocation);
        transform.localPosition = targetLocation;
        fastScrollingLength = Mathf.Sign(fastScrollingLength) * (Mathf.Abs(fastScrollingLength) - speedDump * Time.deltaTime);

        if (Mathf.Abs(fastScrollingLength) <= 10)
        {
            fastScrollingLength = 0;
            fastScrolling = false;
        }
    }

    // checks if the player is scrolling fast
    private void fastScrollingCheck()
    {
        if (Mathf.Abs(previousY - currentY) > fastScrollingThreshold)
        {
            fastScrolling = true;
            fastScrollingLength = currentY - previousY;
        }
        else
        {
            fastScrolling = false;
            fastScrollingLength = 0;
        }
    }

    // checks if the player hit one of the scrolling limits
    private void checkLimitSwitches()
    {
        if (transform.localPosition.y <= minY)
        {
            upperLimitSwitch = true;
        }
        else { upperLimitSwitch = false; }

        if (transform.localPosition.y >= maxY)
        {
            lowerLimitSwitch = true;
        }
        else { lowerLimitSwitch = false; }

    }

    // confines the movement into the given limits
    private void checkTargetLocation( ref Vector3 targetLocation )
    {
        if (upperLimitSwitch) { targetLocation.y = Mathf.Max( targetLocation.y, minY ); }

        else if(lowerLimitSwitch) { targetLocation.y = Mathf.Min(targetLocation.y, maxY); }
    }

    private void Update()
    {
        if(isEnabled)
        {
            checkLimitSwitches();

            if (input.IsPressed())
            {
                if (!pressed) { pressedStatusSwitch(); }

                scroll();
                fastScrollingCheck();
                previousY = currentY;
            }
            else
            {
                if (pressed) { pressedStatusSwitch(); }

                if(fastScrolling) { inertialScroll(); }

            }
        }

    }

}
